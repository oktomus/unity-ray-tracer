﻿
namespace GPURaytracer
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    [RequireComponent(typeof(Camera))]
    public class Raytracer : MonoBehaviour
    {
        #region PROCEDURAL_OBJECTS
        private struct Sphere
        {
            public float radius;
            public Vector3 position;
            public float smoothness;
            public Vector3 albedo;
            public Vector3 specular;
            public Vector3 emission;
        };
        private static readonly int SphereStride = sizeof(float) * 14;
        #endregion

        #region MESH_OBJECT
        struct MeshObject
        {
            public Matrix4x4 localToWorldMatrix;
            public int indicesOffset;
            public int indicesCount;
        }
        #endregion

        #region MASTER_STATIC_PROPERTIES_AND_METHODS
        private static bool _meshesNeedRebuild = false;
        private static List<RaytracerMesh> _meshes = new List<RaytracerMesh>();

        public static void RegisterObject(RaytracerMesh obj)
        {
            _meshes.Add(obj);
            _meshesNeedRebuild = true;
        }

        public static void UnregisterObject(RaytracerMesh obj)
        {
            _meshes.Remove(obj);
            _meshesNeedRebuild = true;
        }
        #endregion

        /// <summary>The compute shader to use for ray tracing.</summary>
        [SerializeField, Tooltip("Rendering compute shader")]
        private ComputeShader _rayTracingShader = null;

        /// <summary>The sky or environment texture.</summary>
        [SerializeField, Tooltip("Skybox")]
        private Texture _skyboxTexture = null;

        /// <summary>The directional light of the scene.</summary>
        [SerializeField, Tooltip("Sun directional light")]
        private Light _sun = null;

        [SerializeField, Tooltip("RNG seed")]
        private int _rngSeed = 0;

        private Camera _camera = null;

        /// <summary>Frame buffer where the scene is rendered before being sent to the screen.</summary>
        private RenderTexture _buffer = null;
        
        private RenderTexture _converged = null;

        /// <summary>Shader addings pixel samples together.</summary>
        private Material _addSampleMaterial = null;

        /// <summary>Number of samples so far (per pixel).</summary>
        private uint _sampleCount = 0;

        #region SCENE_DESCRIPTION_PROPERTIES
        /// <summary>Buffer containing spheres to render.</summary>
        private ComputeBuffer _sphereBuffer = null;

        /// <summary>Buffer containing triangular mesh instances to render.</summary>
        private static List<MeshObject> _meshObjects = new List<MeshObject>();
        private ComputeBuffer _meshObjectBuffer = null;

        /// <summary>Buffer of all triangles in the scene.</summary>
        private static List<Vector3> _vertices = new List<Vector3>();
        private ComputeBuffer _vertexBuffer = null;

        /// <summary>Buffer of meshes faces.</summary>
        private static List<int> _indices = new List<int>();
        private ComputeBuffer _indexBuffer = null;
        #endregion

        #region MONOBEHVAIOUR
        private void Awake()
        {
            _camera = GetComponent<Camera>();
            _addSampleMaterial = new Material(Shader.Find("Hidden/Raytracer/AddSample"));

            Debug.Assert(_rayTracingShader != null && _skyboxTexture != null && _sun != null, "Members are not correctly initialized.");
        }

        private void OnDisable()
        {
            if (_sphereBuffer != null)
                _sphereBuffer.Release();

            if (_meshObjectBuffer != null)
                _meshObjectBuffer.Release();

            if (_vertexBuffer != null)
                _vertexBuffer.Release();

            if (_indexBuffer != null)
                _indexBuffer.Release();
        }

        private void OnEnable()
        {
            SetupScene();
        }

        private void OnRenderImage(RenderTexture source, RenderTexture destination)
        {
            Render(destination);
        }

        private void Update()
        {
            if (transform.hasChanged)
            {
                _sampleCount = 0;
                transform.hasChanged = false;
            }

            if (_sun.transform.hasChanged)
            {
                _sampleCount = 0;
                _sun.transform.hasChanged = false;
            }
        }
        #endregion // MONOBEHAVIOUR

        #region MANAGEMENT
        private void SetupScene()
        {
            Random.InitState(_rngSeed);

            List<Sphere> spheres = new List<Sphere>();

            for (int i = 0; i < 200; ++i)
            {
                Sphere sphere = new Sphere();

                sphere.radius = 2.0f + Random.value * 6.0f;
                Vector2 randomPos = Random.insideUnitCircle * 50.0f;
                sphere.position = new Vector3(randomPos.x, sphere.radius, randomPos.y);

                foreach (Sphere other in spheres)
                {
                    float minDist = sphere.radius + other.radius;
                    if (Vector3.SqrMagnitude(sphere.position - other.position) < minDist * minDist)
                        goto SkipSphere;
                }

                sphere.smoothness = Random.value;

                Color color = Random.ColorHSV();
                bool metal = Random.value < 0.0f;
                sphere.albedo = metal ? Vector3.zero : new Vector3(color.r, color.g, color.b);
                sphere.specular = metal ? new Vector3(color.r, color.g, color.b) : Vector3.one * 0.04f;
                bool emitive = Random.value < 0.2f;
                sphere.emission = emitive ? Vector3.one : Vector3.zero;

                spheres.Add(sphere);

            SkipSphere:
                continue;
            }

            // Assign to compute buffer
            _sphereBuffer = new ComputeBuffer(spheres.Count, SphereStride);
            _sphereBuffer.SetData(spheres);
        }

        private void RebuildMeshesBuffer()
        {
            if (!_meshesNeedRebuild)
                return;

            _meshesNeedRebuild = false;
            _sampleCount = 0;

            _meshObjects.Clear();
            _vertices.Clear();
            _indices.Clear();

            foreach (RaytracerMesh rtMesh in _meshes)
            {
                Mesh mesh = rtMesh.GetComponent<MeshFilter>().sharedMesh;

                // Add vertex data.
                int firstVertex = _vertices.Count;
                _vertices.AddRange(mesh.vertices);

                // Add indice data (with offset in case there is multiple meshes).
                int firstIndex = _indices.Count;
                var indices = mesh.GetIndices(0);
                _indices.AddRange(indices.Select(index => index + firstVertex));

                // Add the mesh instance.
                _meshObjects.Add(new MeshObject()
                {
                    localToWorldMatrix = rtMesh.transform.localToWorldMatrix,
                    indicesOffset = firstIndex,
                    indicesCount = indices.Length
                });
            }

            CreateComputeBuffer(ref _meshObjectBuffer, _meshObjects, 72);
            CreateComputeBuffer(ref _vertexBuffer, _vertices, 12);
            CreateComputeBuffer(ref _indexBuffer, _indices, 4);
        }
        #endregion // MANAGEMENT

        #region RENDERING
        /// <summary>Render the scene with path tracing into the given buffer.</summary>
        private void Render(RenderTexture destination)
        {
            RebuildMeshesBuffer();

            // Initialize a buffer if we don't have one.
            InitFramebufferIfRequired(ref _buffer);

            SetComputeShaderParameters();

            // Dispatch GPU jobs.
            int threadGroupsX = Mathf.CeilToInt(Screen.width / 8.0f);
            int threadGroupsY = Mathf.CeilToInt(Screen.height / 8.0f);
            _rayTracingShader.Dispatch(0, threadGroupsX, threadGroupsY, 1);

            // Blit the render into the given destination buffer.
            _addSampleMaterial.SetFloat("_Sample", _sampleCount);
            Graphics.Blit(_buffer, _converged, _addSampleMaterial);
            Graphics.Blit(_converged, destination);
            _sampleCount++;
        }

        /// <summary>Send camera matrices to the rendering shader.</summary>
        private void SetComputeShaderParameters()
        {
            // Send the kernel attributes.
            _rayTracingShader.SetTexture(0, "_Framebuffer", _buffer);            
            _rayTracingShader.SetMatrix("_CameraToWorld", _camera.cameraToWorldMatrix);
            _rayTracingShader.SetMatrix("_CameraInverseProjection", _camera.projectionMatrix.inverse);
            _rayTracingShader.SetVector("_PixelOffset", new Vector2(Random.value, Random.value));

            // Send the scene.
            _rayTracingShader.SetTexture(0, "_SkyboxTexture", _skyboxTexture);
            Vector3 sunForward = _sun.transform.forward;
            _rayTracingShader.SetVector("_SunLight", new Vector4(sunForward.x, sunForward.y, sunForward.z, _sun.intensity));
            _rayTracingShader.SetFloat("_Seed", Random.value);
            SetComputeBuffer("_Spheres", _sphereBuffer);
            SetComputeBuffer("_MeshObjects", _meshObjectBuffer);
            SetComputeBuffer("_Vertices", _vertexBuffer);
            SetComputeBuffer("_Indices", _indexBuffer);
        }
        #endregion // RENDERING

        #region UTILITIES
        private void SetComputeBuffer(string name, ComputeBuffer buffer)
        {
            if (buffer != null)
                _rayTracingShader.SetBuffer(0, name, buffer);
        }

        /// <summary>Create or reset the buffer if the buffer we want does not have the correct size. </summary>
        private static void CreateComputeBuffer<T>(ref ComputeBuffer buffer, List<T> data, int stride) where T : struct
        {
            if (buffer != null)
            {
                if (data.Count == 0 || buffer.count != data.Count || buffer.stride != stride)
                {
                    buffer.Release();
                    buffer = null;
                }
            }

            if (data.Count != 0)
            {
                if (buffer == null)
                    buffer = new ComputeBuffer(data.Count, stride);

                buffer.SetData(data);
            }
        }

        /// <summary>Checks if the given buffer needs to be created or updated according to the screen size.</summary>
        private static void InitFramebufferIfRequired(ref RenderTexture buffer)
        {
            if (!buffer || buffer.width != Screen.width || buffer.height != Screen.height)
            {
                // Release a previously created buffer.
                if (buffer)
                {
                    buffer.Release();
                }

                // Create the frame buffer.
                buffer = new RenderTexture(
                    Screen.width,
                    Screen.height,
                    0,
                    RenderTextureFormat.ARGBFloat,
                    RenderTextureReadWrite.Linear);
                buffer.enableRandomWrite = true;
                buffer.Create();
            }
        }
        #endregion // UTILITIES
    }
}