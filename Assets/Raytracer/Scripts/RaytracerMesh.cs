﻿using UnityEngine;

namespace GPURaytracer
{
    [RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
    public class RaytracerMesh : MonoBehaviour
    {
        private void OnEnable()
        {
            Raytracer.RegisterObject(this);
        }
        private void OnDisable()
        {
            Raytracer.UnregisterObject(this);
        }
    }
}
